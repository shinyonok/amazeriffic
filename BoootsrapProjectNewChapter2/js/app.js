/**
 * Created by Shinyonok on 05.02.2015.
 */
var main = function () {
    "use strict";

    var toDos = [
        "Закончить писать эту книгу",
        "Вывести Гейси на прогулку в парк",
        "Ответить на электронные письма",
        "Подготовиться к лекции в понедельник",
        "Обновить несколько новых задач",
        "Купить продукты"
    ];

/**    var makeTabActive = function (tabNumber) {
        var tabSelector = ".tabs a:nth-child("+ tabNumber +") span";
        $(".tabs span").removeClass("myActive");
        $(tabSelector).addClass("myActive");
    };

    $(".tabs a:nth-child(1)").on("click", function () {
        makeTabActive(1);
        return false;
    });

    $(".tabs a:nth-child(2)").on("click", function () {
        makeTabActive(2);
        return false;
    });

    $(".tabs a:nth-child(3)").on("click", function () {
        makeTabActive(3);
        return false;
    });

    console.log("Test"); **/

    $(".tabs span").toArray().forEach(function (element){
        $(element).on("click", function() {
            var $element = $(element);
            $(".tabs a span").removeClass("myActive");
            $(element).addClass("myActive");
            $("main .content").empty();
            if ($element.parent().is(":nth-child(1)")) {
                console.log("1");
                for (var newtodo = toDos.length ; newtodo >= 0; newtodo--) {
                    $("main .content").append($("<li>").text(toDos[newtodo]));
                }
            }
            else if ($element.parent().is(":nth-child(2)")) {
                console.log("2");
                toDos.forEach(function(todo){
                    $("main .content").append($("<li>").text(todo));
                });
            }
            else if ($element.parent().is(":nth-child(3)")) {
                console.log("3");
                $("main .content").append($("<h4>").text("Ваш комменатрий:"));
                $("main .content").append($("<input>").addClass("form-control"));
                $("main .content").append($("<button>").addClass("btn btn-default").text("+"));
            }
            return false;
        });
    });

    $(".tabs a:first-child span").trigger("click");
};

$(document).ready(main);